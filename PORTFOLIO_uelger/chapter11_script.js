function calculateBMI() {
    var weightInput = document.getElementById('weight');
    var heightInput = document.getElementById('height');
    var resultDiv = document.getElementById('result');
    

    var weight = weightInput.value;
    var height = heightInput.value / 100; 

    if (weight === '' || height === '') {
        resultDiv.innerHTML = '<div class="alert alert-danger">Please enter valid values!</div>';
    } else {
        var bmi = calculateBMIScore(weight, height);
        var category = getBMICategory(bmi);
       

        var resultText = 'Your BMI: ' + bmi.toFixed(2) + ' (' + category + ').';
        resultDiv.innerHTML = '<div class="alert alert-success">' + resultText + '</div>';
    }
   
}

function calculateBMIScore(weight, height) {
    return weight / (height * height);
}

function getBMICategory(bmi) {
    if (bmi < 18.5) {
        return 'Underweight';
    } else if (bmi >= 18.5 && bmi < 25) {
        return 'Normal weight';
    } else if (bmi >= 25 && bmi < 30) {
        return 'Overweight';
    } else {
        return 'Obesity!!';
    }
}

