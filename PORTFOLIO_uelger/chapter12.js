var myElement = document.getElementById('myElement');

function changeElementProperties() {
  myElement.textContent = '';
  
  myElement.style.backgroundColor = 'blue';
  myElement.style.width = '200px';
  myElement.style.height = '200px';

  myElement.animate(
    [
      { transform: 'rotate(0deg)' },
      { transform: 'rotate(360deg)' }
    ],
    {
      duration: 2000,
      iterations: Infinity
    }
  );
}

myElement.addEventListener('click', () => {
  myElement.textContent = '';
  changeElementProperties();
});
